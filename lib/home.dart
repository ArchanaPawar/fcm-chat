import 'dart:async';
import 'dart:convert';
import 'dart:io';
//https://gitlab.com/tekzee/ambulance-customer/-/blob/5eace8009467a2bf49ffe29309ec88ab9c847f48/lib/src/firebase_notification_handler/firebase_notification_handler.dart
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_demo/chat.dart';
import 'package:flutter_chat_demo/const.dart';
import 'package:flutter_chat_demo/settings.dart';
import 'package:flutter_chat_demo/widget/loading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart';

class HomeScreen extends StatefulWidget {
  final String currentUserId;
  HomeScreen({Key key, @required this.currentUserId}) : super(key: key);
  @override
  State createState() => HomeScreenState(currentUserId: currentUserId);
}

class HomeScreenState extends State<HomeScreen> {

  HomeScreenState({Key key, @required this.currentUserId});
  final String currentUserId;
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final ScrollController listScrollController = ScrollController();
  int _limit = 20;
  int _limitIncrement = 20;
  bool isLoading = false;
  List<Choice> choices = const <Choice>[
    const Choice(title: 'Settings', icon: Icons.settings),
    const Choice(title: 'Log out', icon: Icons.exit_to_app),
  ];

  @override
  void initState() {
    super.initState();
    registerNotification();
    configLocalNotification();
    listScrollController.addListener(scrollListener);
  }

  void handleNotification(Map<String, dynamic> message)
  {
    print(message.toString());
    try
    {
      Platform.isAndroid ? showNotification(message) : showNotification(message);
      bookingNotification(message, context);

      /*String notificationType = message["data"]["type"];
      // _showNotificationWithDefaultSound(message);
      switch (notificationType) {
        case "0":
          break;

        case "1":
          print("booking notification");
          bookingNotification(message, mcontext);
          break;

        case "2":
          break;
      }*/
    } catch (exception) {
      print("exception------>" + exception.toString());
    }
  }

  static handleBackgroundNotification(Map<String, dynamic> message) {

    Platform.isAndroid ? showNotification(message['notification']) : showNotification(message['aps']['alert']);

    /* try {
      String notificationType = message["data"]["type"];

      switch (notificationType) {
        case "0":
          break;

        case "1":
          print("showing dialog");
          //bookingNotification(message, mcontext);
          break;

        case "2":
          break;
      }
    } catch (exception) {
      print("exception------>" + exception.toString());
    }*/
  }

   handleForGroundNotification(Map<String, dynamic> message) {
    print(message);

    // final Item item = _itemForMessage(message);
     // Clear away dialogs
     //Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);

     Navigator.push(context, MaterialPageRoute(builder: (context) => ChatSettings()));
     //Navigator.push(context, ChatSettings());
    Platform.isAndroid ? showNotification(message['notification']) : showNotification(message['aps']['alert']);

//    Utility.log("notification", message["notification"]);
//    Utility.log("data", message["data"]);

    try {

      String notificationType = message["data"]["type"];

      switch (notificationType) {
        case "0":
          break;
        case "1":
//          HomeScreen screen = HomeScreen();
//
//          Navigator.pushNamed(mcontext, "home");
//
//          screen.createState().initState();
//
//          print("showing dialog");
//
//          Utility.showAlertDialog(mcontext, message["data"]["title"]);
          break;

        case "2":
          break;
      }
    } catch (exception) {
      print("exception------>" + exception.toString());
    }
  }

   void bookingNotification(Map<String, dynamic> message, BuildContext ctx) {
   /*
    try
    {
      BookingNotification notification = BookingNotification.fromMap(message);
      String bookingStatus = notification.data.subType;

      if (notification.data.bookingType == "2") {
      } else {
        print("bookingType -1");
        switch (bookingStatus) {
          case "1":
            print("bookingStatus -1");
            Navigator.of(myGlobals.scaffoldKey.currentContext).pop();
            Navigator.of(myGlobals.scaffoldKey.currentContext).pop();
            Navigator.push(myGlobals.scaffoldKey.currentContext, MaterialPageRoute(builder: (context) {
              return CurrentBookingActivity(
                bookingId: notification.data.bookingId,
                bookingType: notification.data.bookingType,
              );
            }));
            break;

          case "3":
            print("bookingStatus -3");
            Navigator.of(context).pop();
            Navigator.push(context, MaterialPageRoute(builder: (context)
            {
              return ReviewRatting(
                bookingId: notification.data.bookingId,
                driverDoctorId: notification.data.driverId,
                name: notification.data.name,
                type: notification.data.bookingType,
              );
            }));
            break;

          case "2":
            print("bookingStatus -2");
            Navigator.of(mcontext).pop();
            Navigator.push(mcontext, MaterialPageRoute(builder: (context) {
              return CurrentBookingActivity(
                bookingId: notification.data.bookingId,
                bookingType: notification.data.bookingType,
              );
            }));
            break;

          case "5":
            Navigator.of(ctx).pop();
            Utility.showAlertDialog(ctx, notification.notification.body);
            break;
        }
      }
    } catch (exception) {
      print("exception------>" + exception.toString());
    }
    */
  }

 static  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    try
    {
      if (message.containsKey('data')) {
        // Handle data message
        final dynamic data = message['data'];
        print("data ---> $data");
        handleBackgroundNotification(message);
      }

      if (message.containsKey('notification')) {
        // Handle notification message
        final dynamic notification = message['notification'];
      }
    }catch(e)
     {

     }
    // Or do other work.
  }

  void registerNotification()
  {
      firebaseMessaging.requestNotificationPermissions();
      firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async
        {
          print('on message ----> $message');
          handleNotification(message);
        },
        onResume: (Map<String, dynamic> message) async
        {
          print('on resume ----> $message');
          handleBackgroundNotification(message);
        },
        onLaunch: (Map<String, dynamic> message) async
        {
          handleForGroundNotification(message);
          print('on launch ----> $message');
        },
        onBackgroundMessage: myBackgroundMessageHandler,
      );
      firebaseMessaging.getToken().then((token)
      {
        print('token: $token');
        FirebaseFirestore.instance.collection('users').doc(currentUserId).update({'pushToken': token});
      }).catchError((err) {
        Fluttertoast.showToast(msg: err.message.toString());
      });
/*
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
      print('onMessage: $message');
      Platform.isAndroid ? showNotification(message['notification']) : showNotification(message['aps']['alert']);
      return;
    },
      onResume: (Map<String, dynamic> message) {
      print('onResume: $message');
      return;
    },
      onLaunch: (Map<String, dynamic> message) {
      print('onLaunch: $message');
      return;
    },
    );

    */
  }

  void configLocalNotification() {
    var initializationSettingsAndroid = new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
   // flutterLocalNotificationsPlugin.initialize(initializationSettings);
    flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: selectNotification);
  }

  void scrollListener() {
    if (listScrollController.offset >= listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange)
    {
      setState(() {
        _limit += _limitIncrement;
      });
    }
  }

  void onItemMenuPress(Choice choice)
  {
    if (choice.title == 'Log out')
    {
      handleSignOut();
    } else
    {
      Navigator.push(context, MaterialPageRoute(builder: (context) => ChatSettings()));
    }
  }

  Future selectNotification(message) async
  {
    print(message);
    try
    {
      var data = json.decode(message.toString());
      print(data["id"].toString());
   /*   Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomeScreen(currentUserId:currentUserId,)));
   */   Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Chat(
                peerId: data["sendid"].toString(),
                peerAvatar: data['photoUrl'],
                docdata: data,
              )));
    }catch(e)
    {
      print(e);
    }
/*
   //print();

    */


  }
 static void showNotification(message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      Platform.isAndroid ? 'com.dfa.flutterchatdemonew' : 'com.duytq.flutterchatdemo',
      'Flutter chat demo',
      'your channel description',
      playSound: true,
      enableVibration: true,
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    print(message);
//    print(message['body'].toString());
//    print(json.encode(message));

    await flutterLocalNotificationsPlugin.show(
        0, message["notification"]['title'].toString(), message["notification"]['body'].toString(), platformChannelSpecifics,
        payload: message["data"]["profile"].toString());
}

  Future<bool> onBackPress() {
    openDialog();
    return Future.value(false);
  }

  Future<Null> openDialog() async {
    switch (await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
            children: <Widget>[
              Container(
                color: themeColor,
                margin: EdgeInsets.all(0.0),
                padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                height: 100.0,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    ),
                    Text(
                      'Exit app',
                      style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Are you sure to exit app?',
                      style: TextStyle(color: Colors.white70, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 0);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.cancel,
                        color: primaryColor,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'CANCEL',
                      style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 1);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.check_circle,
                        color: primaryColor,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'YES',
                      style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          );
        })) {
      case 0:
        break;
      case 1:
        exit(0);
        break;
    }
  }

  Future<Null> handleSignOut() async {
    this.setState(() {
      isLoading = true;
    });


    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("login", "0");
    await FirebaseAuth.instance.signOut();
    await googleSignIn.disconnect();
    await googleSignIn.signOut();

    this.setState(() {
      isLoading = false;
    });

    Navigator.of(context)
        .pushAndRemoveUntil(MaterialPageRoute(builder: (context) => MyApp()), (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'MAIN',
          style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: <Widget>[

          PopupMenuButton<Choice>(
            onSelected: onItemMenuPress,
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
                return PopupMenuItem<Choice>(
                    value: choice,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          choice.icon,
                          color: primaryColor,
                        ),
                        Container(
                          width: 10.0,
                        ),
                        Text(
                          choice.title,
                          style: TextStyle(color: primaryColor),
                        ),
                      ],
                    ));
              }).toList();
            },
          ),
        ],
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            // List
            Container(
              child: StreamBuilder(
                stream: FirebaseFirestore.instance.collection('users').limit(_limit).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) => buildItem(context, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                      controller: listScrollController,
                    );
                  }
                },
              ),
            ),

            // Loading
            Positioned(
              child: isLoading ? const Loading() : Container(),
            )
          ],
        ),
        onWillPop: onBackPress,
      ),
    );
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    if (document.data()['id'] == currentUserId) {
      return Container();
    } else {
      return Container(
        child: FlatButton(
          child: Row(
            children: <Widget>[
              Material(
                child: document.data()['photoUrl'] != null
                    ? CachedNetworkImage(
                        placeholder: (context, url) => Container(
                          child: CircularProgressIndicator(
                            strokeWidth: 1.0,
                            valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                          ),
                          width: 50.0,
                          height: 50.0,
                          padding: EdgeInsets.all(15.0),
                        ),
                        imageUrl: document.data()['photoUrl'],
                        width: 50.0,
                        height: 50.0,
                        fit: BoxFit.cover,
                      )
                    : Icon(
                        Icons.account_circle,
                        size: 50.0,
                        color: greyColor,
                      ),
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                clipBehavior: Clip.hardEdge,
              ),
              Flexible(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Nickname: ${document.data()['nickname']}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                      ),
                      Container(
                        child: Text(
                          'About me: ${document.data()['aboutMe'] ?? 'Not available'}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(left: 20.0),
                ),
              ),
            ],
          ),
          onPressed: () {
            print(document.id);
            //print(document.data());
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Chat(
                          peerId: document.id,
                          peerAvatar: document.data()['photoUrl'],
                          docdata: document.data(),
                        )));
          },
          color: greyColor2,
          padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
      );
    }
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}
